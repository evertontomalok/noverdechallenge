# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot

snapshots = Snapshot()

snapshots['test_get_url 1'] = {
    'test': 123
}

snapshots['test_get_score 1'] = 700

snapshots['test_get_score_url_blocked 1'] = 600

snapshots['test_get_commitment_url_blocked 1'] = 0.5

snapshots['test_get_commitment 1'] = 0.3
