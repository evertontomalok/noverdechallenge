import responses

from api.controllers import get_commitment, get_score, get_url
from api.exceptions import RetrievingValueError


@responses.activate
def test_get_url(snapshot):
    url = "http://example.com"
    method = "GET"

    responses.add(
        responses.Response(method=method, url=url, status=200, json={"test": 123})
    )

    res = get_url(url, method)

    snapshot.assert_match(res.json())


@responses.activate
def test_get_score(snapshot):
    url = "https://challenge.noverde.name/score"
    method = "POST"

    responses.add(
        responses.Response(method=method, url=url, status=200, json={"score": 700})
    )

    score = get_score("12345678900")

    snapshot.assert_match(score)


@responses.activate
def test_get_score_url_blocked(snapshot):
    url = "https://challenge.noverde.name/score"
    method = "POST"

    responses.add(
        responses.Response(
            method=method, url=url, status=200, json={"message": "Limit Exceeded"}
        )
    )

    score = get_score("12345678900")

    snapshot.assert_match(score)


@responses.activate
def test_get_score_some_trouble_in_request():
    url = "https://challenge.noverde.name/score"
    method = "POST"

    responses.add(responses.Response(method=method, url=url, status=500))

    try:
        get_score("12345678900")
        assert False
    except RetrievingValueError:
        assert True


@responses.activate
def test_get_commitment(snapshot):
    url = "https://challenge.noverde.name/commitment"
    method = "POST"

    responses.add(
        responses.Response(method=method, url=url, status=200, json={"commitment": 0.3})
    )

    score = get_commitment("12345678900")

    snapshot.assert_match(score)


@responses.activate
def test_get_commitment_url_blocked(snapshot):
    url = "https://challenge.noverde.name/commitment"
    method = "POST"

    responses.add(
        responses.Response(
            method=method, url=url, status=200, json={"message": "Limit Exceeded"}
        )
    )

    score = get_commitment("12345678900")

    snapshot.assert_match(score)


@responses.activate
def test_get_commitment_some_trouble_in_request():
    url = "https://challenge.noverde.name/commitment"
    method = "POST"

    responses.add(responses.Response(method=method, url=url, status=500))

    try:
        get_commitment("12345678900")
        assert False
    except RetrievingValueError:
        assert True
