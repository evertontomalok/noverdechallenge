import json
import logging
from json.decoder import JSONDecodeError
from os import getenv

import backoff
import requests

from api.exceptions import RetrievingValueError

logger = logging.getLogger()

api_key = getenv("API_KEY")


@backoff.on_exception(
    backoff.expo,
    (requests.exceptions.Timeout, requests.exceptions.ConnectionError),
    max_tries=3,
    max_time=300,
)
def get_url(url: str, method: str, payload=None, headers=None):
    payload = payload or {}
    response = requests.request(method, url, headers=headers, data=json.dumps(payload))
    return response


def get_score(cpf: str) -> int:
    url = "https://challenge.noverde.name/score"
    payload = {"cpf": cpf}
    headers = {"x-api-key": api_key, "Content-Type": "text/plain"}

    try:
        response_json = get_url(url, "POST", payload, headers).json()
        if response_json.get("score"):
            return response_json.get("score")
        elif response_json.get("message"):
            """
                This piece of code is not necessary if the API doesn't have a
                limit requests or it blocks the requisition. This is here only
                to not stopping the processing, because it is a simple application
                demonstration.
            """
            logger.info(
                "It's not possible to get a real value from api. Using the lower "
                "value to score."
            )
            return 600
    except (ValueError, JSONDecodeError):
        pass
    raise RetrievingValueError("Score")


def get_commitment(cpf: str) -> float:
    url = "https://challenge.noverde.name/commitment"
    payload = {"cpf": cpf}
    headers = {"x-api-key": api_key, "Content-Type": "text/plain"}

    try:
        response_json = get_url(url, "POST", payload, headers).json()
        if response_json.get("commitment"):
            return response_json.get("commitment")
        elif response_json.get("message"):
            """
                This piece of code is not necessary if the API doesn't have a 
                limit requests or it blocks the requisition. This is here only 
                to not stopping the processing, because it is a simple 
                application demonstration.
            """
            logger.info(
                "It's not possible to get a real value from api. Assuming "
                "0.5 to commitment."
            )
            return 0.5
    except (ValueError, JSONDecodeError):
        pass
    raise RetrievingValueError("Commitment")
